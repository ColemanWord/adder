package design

import (
    . "github.com/goadesign/goa/design"
    . "github.com/goadesign/goa/design/apidsl"
)
// Define the API
var _ = API("sum", func() {
        Title("A Sum Service")
        Description("Add two numbers!")
        Host("localhost:8080")
        Scheme("http")
})
// Create a Resource and Action
var _ = Resource("opperands", func() {
    Action("add", func() {
        Routing(GET("add/:left/:right"))
        Description("add two numbers")
        Params(func() {
            Param("left", Integer, "Left operand")
            Param("right", Integer, "Right operand")
        })
    Response(OK, "text/plain")
    })
})

// run goagen bootstrap -d adder/design 
